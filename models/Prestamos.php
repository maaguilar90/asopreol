<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestamos".
 *
 * @property string $cod_cliente
 * @property string $ruc_ci
 * @property string $nombres
 * @property string $documento
 * @property string $capital
 * @property string $tasa
 * @property string $plazomeses
 * @property string $fechaadj
 * @property string $capitalpagado
 * @property string $detalle
 * @property string $tipoprestamo
 * @property string $fechaingreso
 */
class Prestamos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prestamos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cod_cliente'], 'required'],
            [['fechaadj', 'fechaingreso'], 'safe'],
            [['cod_cliente', 'capital', 'tasa', 'plazomeses', 'capitalpagado'], 'string', 'max' => 20],
            [['ruc_ci'], 'string', 'max' => 10],
            [['nombres'], 'string', 'max' => 300],
            [['documento', 'tipoprestamo'], 'string', 'max' => 100],
            [['detalle'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cod_cliente' => 'Cod Cliente',
            'ruc_ci' => 'Ruc Ci',
            'nombres' => 'Nombres',
            'documento' => 'Documento',
            'capital' => 'Capital',
            'tasa' => 'Tasa',
            'plazomeses' => 'Plazomeses',
            'fechaadj' => 'Fechaadj',
            'capitalpagado' => 'Capitalpagado',
            'detalle' => 'Detalle',
            'tipoprestamo' => 'Tipoprestamo',
            'fechaingreso' => 'Fechaingreso',
        ];
    }
}
