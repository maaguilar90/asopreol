<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property string $secciontipo
 * @property string $subseccion
 * @property string $titulo
 * @property resource $contenido
 * @property string $imagen
 * @property string $creation_date
 * @property string $estatus
 */
class Contenido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['secciontipo', 'titulo', 'contenido'], 'required'],
            [['secciontipo', 'subseccion', 'contenido', 'estatus'], 'string'],
            [['creation_date'], 'safe'],
            [['titulo'], 'string', 'max' => 300],
            [['imagen'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'secciontipo' => 'Secciontipo',
            'subseccion' => 'Subseccion',
            'titulo' => 'Titulo',
            'contenido' => 'Contenido',
            'imagen' => 'Imagen',
            'creation_date' => 'Creation Date',
            'estatus' => 'Estatus',
        ];
    }
}
