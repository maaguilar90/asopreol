<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserController represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['identity', 'type', 'address_home', 'phone_home', 'cellphone', 'cellphone_company', 'username', 'password', 'country_origin', 'province_residence', 'canton_residence', 'zone_residence', 'chief_representative', 'secondary_representative', 'status', 'names', 'lastnames', 'economic_sector', 'internal_rating', 'city_residence', 'type_client', 'status_client', 'auth_key'], 'safe'],
            [['office', 'link'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'office' => $this->office,
            'link' => $this->link,
        ]);

        $query->andFilterWhere(['like', 'identity', $this->identity])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'address_home', $this->address_home])
            ->andFilterWhere(['like', 'phone_home', $this->phone_home])
            ->andFilterWhere(['like', 'cellphone', $this->cellphone])
            ->andFilterWhere(['like', 'cellphone_company', $this->cellphone_company])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'country_origin', $this->country_origin])
            ->andFilterWhere(['like', 'province_residence', $this->province_residence])
            ->andFilterWhere(['like', 'canton_residence', $this->canton_residence])
            ->andFilterWhere(['like', 'zone_residence', $this->zone_residence])
            ->andFilterWhere(['like', 'chief_representative', $this->chief_representative])
            ->andFilterWhere(['like', 'secondary_representative', $this->secondary_representative])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'names', $this->names])
            ->andFilterWhere(['like', 'lastnames', $this->lastnames])
            ->andFilterWhere(['like', 'economic_sector', $this->economic_sector])
            ->andFilterWhere(['like', 'internal_rating', $this->internal_rating])
            ->andFilterWhere(['like', 'city_residence', $this->city_residence])
            ->andFilterWhere(['like', 'type_client', $this->type_client])
            ->andFilterWhere(['like', 'status_client', $this->status_client])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }
}
