<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "configuraciones".
 *
 * @property integer $id
 * @property string $tipo
 * @property string $contenido
 * @property string $fechacreacion
 * @property string $estatus
 */
class Configuraciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'configuraciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo', 'estatus'], 'string'],
            [['fechacreacion'], 'safe'],
            [['contenido'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'contenido' => 'Contenido',
            'fechacreacion' => 'Fechacreacion',
            'estatus' => 'Estatus',
        ];
    }
}
