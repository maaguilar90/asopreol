<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aportes".
 *
 * @property string $cod_cliente
 * @property string $ruc_ci
 * @property string $nombres
 * @property string $fechaemision
 * @property string $documento
 * @property string $aporte
 * @property string $interes
 * @property string $acumulado
 * @property string $observacion
 * @property string $fechacreacion
 */
class Aportes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aportes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cod_cliente', 'ruc_ci'], 'required'],
            [['fechaemision', 'fechacreacion'], 'safe'],
            [['cod_cliente', 'ruc_ci'], 'string', 'max' => 10],
            [['nombres', 'documento', 'aporte', 'interes', 'acumulado'], 'string', 'max' => 100],
            [['observacion'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cod_cliente' => 'Cod Cliente',
            'ruc_ci' => 'Ruc Ci',
            'nombres' => 'Nombres',
            'fechaemision' => 'Fechaemision',
            'documento' => 'Documento',
            'aporte' => 'Aporte',
            'interes' => 'Interes',
            'acumulado' => 'Acumulado',
            'observacion' => 'Observacion',
            'fechacreacion' => 'Fechacreacion',
        ];
    }
}
