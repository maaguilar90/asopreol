<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $cedula
 * @property string $fechacreacion
 * @property string $nombres
 * @property string $telefonoa
 * @property string $telefonob
 * @property string $telefonoc
 * @property string $direccion
 * @property string $mail
 * @property string $lugtrabajo
 * @property string $banco
 * @property string $tipcuenta
 * @property string $cuenta
 * @property string $estado
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cedula'], 'required'],
            [['fechacreacion'], 'safe'],
            [['estado'], 'string'],
            [['cedula', 'telefonoa', 'telefonob', 'telefonoc'], 'string', 'max' => 10],
            [['nombres'], 'string', 'max' => 250],
            [['direccion'], 'string', 'max' => 300],
            [['mail', 'lugtrabajo', 'banco'], 'string', 'max' => 100],
            [['tipcuenta', 'cuenta'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cedula' => 'Cedula',
            'fechacreacion' => 'Fechacreacion',
            'nombres' => 'Nombres',
            'telefonoa' => 'Telefonoa',
            'telefonob' => 'Telefonob',
            'telefonoc' => 'Telefonoc',
            'direccion' => 'Direccion',
            'mail' => 'Mail',
            'lugtrabajo' => 'Lugtrabajo',
            'banco' => 'Banco',
            'tipcuenta' => 'Tipcuenta',
            'cuenta' => 'Cuenta',
            'estado' => 'Estado',
        ];
    }
}
