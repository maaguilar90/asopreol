<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_home')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_home')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cellphone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cellphone_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'canton_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zone_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chief_representative')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondary_representative')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'office')->textInput() ?>

    <?= $form->field($model, 'names')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastnames')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'economic_sector')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput() ?>

    <?= $form->field($model, 'internal_rating')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_client')->dropDownList([ 'CLIENT' => 'CLIENT', 'ADMIN' => 'ADMIN', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status_client')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'INACTIVE' => 'INACTIVE', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
