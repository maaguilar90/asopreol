<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

 

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'identity',
            'type',
            'address_home',
            'phone_home',
            'cellphone',
            // 'cellphone_company',
            // 'username',
            // 'password',
            // 'country_origin',
            // 'province_residence',
            // 'canton_residence',
            // 'zone_residence',
            // 'chief_representative',
            // 'secondary_representative',
            // 'status',
            // 'office',
            // 'names',
            // 'lastnames',
            // 'fullname',
            // 'economic_sector',
            // 'link',
            // 'internal_rating',
            // 'city_residence',
            // 'type_client',
            // 'status_client',
            // 'auth_key',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
