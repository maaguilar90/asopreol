<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->identity;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->identity], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->identity], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'identity',
            'type',
            'address_home',
            'phone_home',
            'cellphone',
            'cellphone_company',
            'username',
            'password',
            'country_origin',
            'province_residence',
            'canton_residence',
            'zone_residence',
            'chief_representative',
            'secondary_representative',
            'status',
            'office',
            'names',
            'lastnames',
            'fullname',
            'economic_sector',
            'link',
            'internal_rating',
            'city_residence',
            'type_client',
            'status_client',
            'auth_key',
        ],
    ]) ?>

</div>
