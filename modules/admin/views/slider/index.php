<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

 

    <p>
        <?= Html::a('Create Slider', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            array(
                'attribute' => 'Imagen',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img('@web/images/banner/' . $data->image, ['width' => '250']);
                }
            ),
            //'image',
            'orden',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
