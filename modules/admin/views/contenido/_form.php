<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Contenido */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contenido-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'secciontipo')->dropDownList([ 'HOME' => 'HOME', 'CESANTIA' => 'CESANTIA', 'SERVICIOS' => 'SERVICIOS', 'EDUCACIONFINANCIERA' => 'EDUCACIONFINANCIERA', 'TRANSPARENCIA' => 'TRANSPARENCIA', 'CONTACTENOS' => 'CONTACTENOS', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contenido')->widget(TinyMce::className(), [
        'options' => ['rows' => 9],
        'language' => 'es',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste",
                "image"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image | fullscreen"
        ]
    ]);?>

    <?= $form->field($model, 'imagen')->fileInput() ?>
    <?= Html::img('@web/images/site/' . $model->imagen, ['width' => '30%']); ?>

    <?= $form->field($model, 'estatus')->dropDownList([ 'ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
