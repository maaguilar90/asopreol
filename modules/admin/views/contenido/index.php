<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contenidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contenido-index">
    <p>
        <?= Html::a('Create Contenido', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions'=>['style'=>'max-width: 100px;']
            ],

            //'id',
            'secciontipo',
            'titulo',
            [
                'value' => 'contenido', /*function($address) {
                    $name = $address['name'] . ' ' . $address['lastName'];
                    return $name;
                },*/
                'contentOptions'=>['style'=>'max-width: 400px;']
            ],
            'imagen',
            // 'creation_date',
            // 'estatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
