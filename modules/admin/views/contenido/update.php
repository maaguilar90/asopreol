<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contenido */

$this->title = 'Update Contenido: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contenidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contenido-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
