<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Configuraciones */

$this->title = 'Create Configuraciones';
$this->params['breadcrumbs'][] = ['label' => 'Configuraciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="configuraciones-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
