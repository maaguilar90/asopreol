<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use app\themes\adminLTE\components\ThemeNav;

?>
<?php $this->beginContent('@app/themes/adminLTE/layouts/main.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar ">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/user_accounts.png" class="img-circle"
                         alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>
                        <?php
                        $info[] = Yii::t('app', 'Hola');

                        if (isset(Yii::$app->user->identity->username))
                            $info[] = ucfirst(\Yii::$app->user->identity->fullname);
                        echo implode(', ', $info);
                        ?>
                    </p>
                    <a><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?php
            echo Menu::widget([
                'encodeLabels' => false,
                'options' => [
                    'class' => 'sidebar-menu treeview'
                ],
                'items' => [
                    ['label' => Yii::t('app', 'MENU'), 'options' => ['class' => 'header']],
                    //['label' => ThemeNav::link('Dashboard', 'fa fa-dashboard'), 'url' => ['site/index'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => ThemeNav::link('Banners', 'fa fa-book'), 'url' => ['slider/'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => ThemeNav::link('Menú', 'fa fa-book'), 'url' => ['seccion/'], 'visible' => !Yii::$app->user->isGuest],
                    
                    ['label' => ThemeNav::link('Secciones', 'fa fa-book'), 'url' => ['#'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => ThemeNav::link('Home', 'fa fa-search'), 'url' => ['contenido?seccion=home/'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Servicios', 'fa fa-search'), 'url' => ['contenido?seccion=servicios/'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Transparencia', 'fa fa-search'), 'url' => ['contenido?seccion=transparencia/'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Educación Financiera', 'fa fa-search'), 'url' => ['contenido?seccion=educacion/'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Convenios', 'fa fa-search'), 'url' => ['contenido?seccion=convenios/'], 'visible' => !Yii::$app->user->isGuest],
                        ],
                    ],
                    ['label' => ThemeNav::link('Footer', 'fa fa-book'), 'url' => ['configuraciones/'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => ThemeNav::link('Usuarios', 'fa fa-book'), 'url' => ['usuarios/'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => ThemeNav::link('Cerrar sesión', 'fa fa-book'), 'url' => ['/site/logout/'], 'visible' => !Yii::$app->user->isGuest],
                    /*['label' => ThemeNav::link('Footer', 'fa fa-book'), 'url' => ['#'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => ThemeNav::link('Registros Zombies ', 'fa fa-eye'), 'url' => ['site/zombies'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Cupones Zombies', 'fa fa-search'), 'url' => ['site/zcupones'], 'visible' => !Yii::$app->user->isGuest],
                        ],
                    ],
                    ['label' => ThemeNav::link('Ticket Regalón', 'fa fa-book'), 'url' => ['#'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => ThemeNav::link('Registros Tickets ', 'fa fa-eye'), 'url' => ['site/ticket'], 'visible' => !Yii::$app->user->isGuest],
                            ['label' => ThemeNav::link('Cupones Tickets', 'fa fa-search'), 'url' => ['site/tkcupones'], 'visible' => !Yii::$app->user->isGuest],
                        ],
                    ],*/
                ],
                'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
                'encodeLabels' => false,
                'activateParents' => true,
            ]);
            ?>

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> <?php echo Html::encode($this->title); ?> </h1>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>
        <!-- Main content -->
        <section class="content">
            <?php echo $content; ?>
        </section><!-- /.content -->

    </div><!-- /.right-side -->
<?php $this->endContent();?>
<style type="text/css">
.navbar
{
 top: 0%
}
</style>