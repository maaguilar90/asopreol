<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use yii\helpers\Url;
$this->title = 'ASOPREOL | ACTUALIZA TU CLAVE';
$this->params['breadcrumbs'][] = $this->title;
?>
 

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/usericon.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan"></span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br><br>
                <span class="titulo-ef">NUEVA CONTRASEÑA</span><br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                
                
                     <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>
            <?= $form->field($model,'password')->input('password')->label('Contraseña.') ?>
            <?= $form->field($model,'confirmpassword')->input('password')->label('Repita su contraseña.') ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Aceptar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
                 
            </div>

        </div>
    </div><br>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>


<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.form-horizontal .control-label
{
    width: 30%;
}

</style>
