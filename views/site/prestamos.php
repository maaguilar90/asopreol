<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | PRÉSTAMOS';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/prestamos.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan"></span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">PRÉSTAMOS</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br>
                <span class="text-content">“ASOPREOL” siempre pensando en el bienestar y crecimiento de sus partícipes, pone a su disposición una gama de servicios crediticios con las mejores ventajas.
                <br><br>
                Los préstamos generados por “ASOPREOL” se basan en que el fondo se obliga a poner una determinada suma de dinero a disposición de los partícipes para que accedan a las líneas de crédito de acuerdo a los requisitos establecidos en el Reglamento de Préstamos. Destinados al pago de bienes, servicios o gastos no relacionados con una actividad productiva, cual fuente de pago es el ingreso mensual obtenido en sus calidad de participes activos del fondo que generalmente se amortizan en función de un sistema de cuotas fijas
                <br><br>
                Los préstamos a los que nuestros participes pueden acceder son:</span>
                <br>
                  <li style="padding-left: 20px;">Préstamo Emergente</li>
                  <li style="padding-left: 20px;">Préstamo Ordinario</li>
                  <li style="padding-left: 20px;">Préstamo Prendario</li>
                  <li style="padding-left: 20px;">Préstamo Hipotecario</li>
                <br>
                <!--<div class="servicio" style="padding: 0.2%;">
                  <div style="text-align: center;">
                    <span style="font-size: 2px;"></span>
                  </div>
                </div>
                <br>-->
                <div style=""><span class="titulo-ef" style="font-size: 17px;">PRÉSTAMO EMERGENTE</span></div>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <div class="text-content">
                    <table>
                        <thead>
                            <tr>
                                <th>MONTO</th>
                                <th>PLAZO</th>
                                <th>TAZA DE INTERÉS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>100% de la cuenta individual</td>
                                <td>7 años</td>
                                <td>10%</td>
                            </tr>
                        </tbody>
                    </table>
                <br><br></span>
                <div class="servicio" style="padding: 0.5%;">
                  <div style="text-align: center;">
                    <span style="font-size: 17px;">Condiciones</span>
                  </div>
                </div>
                <br>
                <li>El número de años del crédito no deberá superar los 75 años de edad del partícipe.</li>
                <li>Haber aportado tres meses en “ASOPREOL”.</li>
                <li>Solicitud de crédito.</li>
                <li>Presentar los 3 últimos roles de pago.</li>
                <li>El pagaré será firmado por el deudor y cónyuge cuando el participe esté casado(a) o en unión libre.</li>
                <li>Copias a color de cédula y papeleta de votación.</li>
                <li>Copia de un servicio básico.</li>
                <br>
                <span style="color: #1A185C;">Novación:</span><br>
                <span>Deberá estar cancelado el 50% del capital del préstamo anterior y cumplir con los requisitos establecidos.</span>
                <br><br>
                 <div style=""><span class="titulo-ef" style="font-size: 17px;">PRÉSTAMO ORDINARIO</span></div>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <div class="text-content">
                    <table>
                        <thead>
                            <tr>
                                <th>MONTO</th>
                                <th>PLAZO</th>
                                <th>GARANTÍA</th>
                                <th>TAZA DE INTERÉS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>USD 5,000.00<br> (Cinco mil dólares)</td>
                                <td>3 años</td>
                                <td>Personal (Partícipe)</td>
                                <td>10%</td>
                            </tr>
                        </tbody>
                    </table>
                <br><br></span>
                <div class="servicio" style="padding: 0.5%;">
                  <div style="text-align: center;">
                    <span style="font-size: 17px;">Condiciones</span>
                  </div>
                </div>
                <br>
                <li>El número de años del crédito no deberá superar los 75 años de edad del partícipe.</li>
                <li>Haber aportado tres meses en “ASOPREOL”.</li>
                <li>Solicitud de crédito.</li>
                <li>Presentar los 3 últimos roles de pago.</li>
                <li>Garante -Partícipe de Asopreol.</li>
                <li>El pagaré será firmado por el deudor y cónyuge cuando el participe esté casado(a) o en unión libre,firma del garante y cónyuge si este es casado(a).</li>
                <li>Copias a color de cédula y papeleta de votación.</li>
                <li>Copia de un servicio básico.</li>
                <br>
                <span style="color: #1A185C;">Novación:</span><br>
                <span>Deberá estar cancelado el 50% del capital del préstamo anterior y cumplir con los requisitos establecidos.</span>

                <br><br>
                 <div style=""><span class="titulo-ef" style="font-size: 17px;">PRÉSTAMO PRENDARIOS</span></div>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <div class="text-content">
                    <table>
                        <thead>
                            <tr>
                                <th>MONTO</th>
                                <th>PLAZO</th>
                                <th>GARANTÍA</th>
                                <th>TAZA DE INTERÉS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Financiamos el 80%<br> de tu vehículo nuevo</td>
                                <td>4 años</td>
                                <td>Prendaria</td>
                                <td>10%</td>
                            </tr>
                        </tbody>
                    </table>
                <br><br></span>
                <div class="servicio" style="padding: 0.5%;">
                  <div style="text-align: center;">
                    <span style="font-size: 17px;">Condiciones</span>
                  </div>
                </div>
                <br>
                <li>El número de años del crédito no deberá superar los 75 años de edad del partícipe.</li>
                <li>Haber aportado tres meses en “ASOPREOL”.</li>
                <li>Solicitud de crédito.</li>
                <li>Proforma de la casa comercial.</li>
                <li>Presentar los 3 últimos roles de pago.</li>
                <li>El pagaré será firmado por el deudor y cónyuge cuando el partícipe esté casado(a) o en unión libre</li>
                <li>Realizar la contratación del seguro vehicular.</li>
                <li>Copias a color de cédula y papeleta de votación.</li>
                <li>Copia de un servicio básico.</li>
                <br>
                <span style="color: #1A185C;">Novación:</span><br>
                <span>Deberá estar cancelado el 50% del capital del préstamo anterior y cumplir con los requisitos establecidos.</span>

                <br><br>
                 <div style=""><span class="titulo-ef" style="font-size: 17px;">PRÉSTAMO HIPOTECARIO</span></div>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <div class="text-content">
                    <table>
                        <thead>
                            <tr>
                                <th>MONTO</th>
                                <th>PLAZO</th>
                                <th>GARANTÍA</th>
                                <th>TAZA DE INTERÉS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>70%  del avalúo del bien<br>inmueble objeto de la Hipoteca</td>
                                <td>25 años</td>
                                <td>Hipoteca</td>
                                <td>10%</td>
                            </tr>
                        </tbody>
                    </table>
                <br><br></span>
                <div class="servicio" style="padding: 0.5%;">
                  <div style="text-align: center;">
                    <span style="font-size: 17px;">Condiciones</span>
                  </div>
                </div>
                <br>
                <li>El número de años del crédito no deberá superar los 75 años de edad del partícipe.</li>
                <li>Haber aportado tres meses en “ASOPREOL”.</li>
                <li>Solicitud de crédito.</li>
                <li>Presentar los 3 últimos roles de pago.</li>
                <li>El pagaré será firmado por el deudor y cónyuge cuando el partícipe esté casado(a) o en unión libre.</li>
                <li>Escritura original del inmueble.</li>
                <li>Certificado de gravámenes actualizado.</li>
                <li>Impuesto Predial del año en curso.</li>
                <li>Contratar el seguro contra-incendios y lineas aliadas.</li>
                <li>Informe del perito avaluador.</li>
                <li>Copias a color de cédula y papeleta de votación.</li>
                <li>Copia de un servicio básico.</li>
                <br>
                <span style="color: #1A185C;">Novación:</span><br>
                <span>Deberá estar cancelado el 50% del capital del préstamo anterior y cumplir con los requisitos establecidos.</span>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>