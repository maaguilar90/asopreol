<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
$this->title="ASOPREP-FCPC | ".$model->title;
?>
        <!-- Paginas internas -->
        <section class="container pos-relative margins-top-pg-interna">
        	<div class="cont-paginterna">
            	<?php if($model->picture): ?>
                    <img src="<?= URL::base() ?>/images/<?= $model->picture ?>"/>
                <?php endif; ?>
                <h1><?= $model->title ?></h1>
				<?= $model->description ?>
            </div>
            <div id="submenu-interna">
            	<?php foreach($mainsection->sections as $section): ?>
            	<h1><?= $section->title ?></h1>
                <ul>
	                <?php foreach($section->contents as $content): ?>
	                	<li><a href="<?= Url::to(['site/content','id'=>$content->id]) ?>"><?= $content->title ?></a></li>
	    			<?php endforeach; ?>
                </ul>
              	<?php endforeach; ?>
            </div>
        </section>
