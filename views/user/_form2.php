<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
// $script='$(document).ready(function() {
//         $("#edit").on("click", function(e){
//     $("input").prop("readonly", false);
//     });
// });
// ';
// $this->registerJs($script,View::POS_END);
AppAsset::register($this);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="margin-top:10px;margin-bottom:10px;">
        <span>Si eres parte de Asoprep, ingresa tu cédula: </span>
    </div>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 1704236757','pattern' => '\d*']) ?>

     <div id="errorMenssage" style="margin-top:10px;margin-bottom:10px;display:none;color:red;">
        <span>Tu cédula no es parte de nuestros registros, por favor comunícate con nosotros para formar parte de Asoprep a los teléfonos: 2246-153, 2255-112, 2255-113, 2255-122 y 2440-748.</span>
    </div>

    <button id="btnconsultar" type="submit" class="btn btn-success" onclick="javascript:consultar_cedula($('#user-identity').val())">Aceptar</button>

   

    <div id="formulario" style="display:none;">

    <?= $form->field($model, 'names')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastnames')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'address_home')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_home')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 022247845']) ?>

    <?= $form->field($model, 'cellphone')->textInput(['maxlength' => true, 'placeholder'=>'Ej. 0998842367']) ?>

   <!--  <?= $form->field($model, 'cellphone_company')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <!-- <?= $form->field($model, 'country_origin')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'province_residence')->textInput(['maxlength' => true, ]) ?>

    <!-- 'style' => 'text-transform: uppercase' -->

    <?= $form->field($model, 'canton_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'zone_residence')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'chief_representative')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'secondary_representative')->textInput(['maxlength' => true]) ?> -->


    <!-- <?= $form->field($model, 'office')->textInput([]) ?> -->



   <!--  <?= $form->field($model, 'link')->textInput([]) ?> -->

    <?= $form->field($model, 'city_residence')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
    
        
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    
$('#user-identity').keypress(function(e) {
    if(e.which == 13) {
        //console.log('Enter');
      //  consultar_cedula($('#user-identity').val());
    }
});

function consultar_cedula(cedula)
{
    console.log('ajax');
    $.ajax({
       url: '<?php echo Yii::$app->request->baseUrl. 'site/findidentity' ?>',
       type: 'post',
       data: {
                 cedula: cedula
             },
       success: function (data) {
          console.log(data);
          if (data==1)
          {
            $('#formulario').fadeIn();
            //$('.help-block').fadeOut();
            $('#errorMenssage').fadeOut();
            $('#btnconsultar').fadeOut();
            $('#user-names').focus();


          }else{
            //$('#errorMenssage').css("display", "block");
            $('#errorMenssage').fadeIn();
            $('#formulario').fadeOut();
          }
       }
  });
}


</script>