<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$script='$(document).ready(function() {
        $("#edit").on("click", function(e){
    $("input").prop("readonly", false);
    });
});
';
$this->registerJs($script,View::POS_END);
AppAsset::register($this);
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <!-- <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?> -->

    <?= $form->field($model, 'address_home')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'phone_home')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'cellphone')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

   <!--  <?= $form->field($model, 'cellphone_company')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?> -->

    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>


   <!--  <?= $form->field($model, 'country_origin')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?> -->

    <?= $form->field($model, 'province_residence')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'canton_residence')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'zone_residence')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <!-- <?= $form->field($model, 'chief_representative')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?> -->

    <!-- <?= $form->field($model, 'secondary_representative')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?> -->


    <!-- <?= $form->field($model, 'office')->textInput(['readonly'=>'readonly']) ?> -->

    <?= $form->field($model, 'names')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <?= $form->field($model, 'lastnames')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>

    <!-- <?= $form->field($model, 'link')->textInput(['readonly'=>'readonly']) ?> -->

    <?= $form->field($model, 'city_residence')->textInput(['maxlength' => true, 'readonly'=>'readonly']) ?>


    <div class="form-group">
        <a id="edit" class="btn btn-primary">Editar</a> 
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
