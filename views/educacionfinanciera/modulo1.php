<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | EDUCACIÓN FINANCIERA';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/educacionfinancieraint.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan">EDUCACIÓN FINANCIERA</span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">MÓDULO 1</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br><br>
                <div style=""><span class="titulo-plan">SEGURIDAD SOCIAL</span></div>
                <span class="text-content">Es un derecho que le asiste a toda persona a acceder a la protección básica para satisfacer sus necesidades la organización y funcionamiento de la seguridad social se fundamentan en los principios de solidaridad, obligatoriedad, universalidad, equidad, eficiencia, subsidiariedad y suficiencia.
                <br><br>
                Este régimen encierra temas como la salud pública, seguro de riesgos de trabajo, seguro campesino o los planes de pensiones y jubilaciones, enfocados en asegurar niveles mínimos de dignidad de vida a todos los ciudadanos, especialmente cuando envejecemos y se ven reducidas nuestras posibilidades de generar por si mismo fuentes de ingreso estables.
                <br>
                <br>
                <b>Contenido Módulo 1:</b> <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO1_SEGURIDAD_SOCIAL_PRODUCTOS_Y_SERVICIOS_FINANCIEROS.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Seguridad Social</span></a>
                <br>
                <br>
                 
                <span class="text-content">Contenido Educación Financiera:
                <br>
                <div class="descargas">
                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo2">
                    <span class=""> Módulo 2: Acceso a la información</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO2_ACCESO_A_LA_INFORMACION_EL_CREDITO_Y_EL_ENDEUDAMIENTO_RESPONSABLE.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>
                    
                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo3">
                    <span class=""> Módulo 3: Presupuesto familiar</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO1_SEGURIDAD_SOCIAL_PRODUCTOS_Y_SERVICIOS_FINANCIEROS.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo4">
                    <span class=""> Módulo 4: Cesantía, Jubilación, Ahorro e inversión</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO4_CESANTIA_JUBILACION_AHORRO_E_INVERSION.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo5">
                    <span class=""> Módulo 5: Seguridad financiera</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO5_SEGURIDAD_FINANCIERA.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/generalidades"> 
                    <span class="">Generalidades</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/GENERALIDADES_ATENCION_AL_USUARIO_DE_LA_SUPER_DE_BANCOS_GLOSARIO_DE_TERMINOS.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a target="_blank" href="<?= URL::base() ?>/pdf/SIMULADORDECREDITOFCPCCASOPREOL.xlsx"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Simulador de Crédito</span></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                <br>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>