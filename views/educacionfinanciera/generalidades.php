<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | EDUCACIÓN FINANCIERA';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/educacionfinancieraint.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan">EDUCACIÓN FINANCIERA</span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">GENERALIDADES</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br><br>
                <div style=""><span class="titulo-plan">GENERALIDADES DE LA SUBDIRECCIÓN DE ATENCIÓN AL USUARIO DE LA SUPERINTENDENCIA DE BANCOS</span></div>
                <span class="text-content">La Superintendencia de Bancos a través de la Subdirección de Atención al Usuario es la encargada de atender, evaluar, aceptar o denegar, analizar y resolver los reclamos presentados por todas aquellas personas que reúnan la condición de usuarios de servicios financieros en contra de una de las entidades controladas, cuando estos se refieren a sus intereses y derechos y se deriven de los contratos, de la normativa vigente; y, en particular de los principios de transparencia, equidad y de las buenas prácticas y usos financieros.
                <br><br>
                Otra de sus funciones es la de emitir reportes estadísticos a nivel nacional de las solicitudes y consultas del público atendidas por la Superintendencia de Bancos y darlas a conocer a la ciudadanía. 
                <br><br>Funciones de la Subdirección de Atención al Usuario<br><br>
                <div style=""><span>Entre las funciones principales de la Subdirección de Atención al Usuario se encuentran:</span></div>
                  <li style="padding-left: 20px;">Resolver los reclamos presentados por la ciudadanía en contra de las entidades financieras a nivel nacional.</li>
                  <li style="padding-left: 20px;">Solicitar documentos, recabar información y requerir explicaciones de las instituciones financieras controladas sobre operaciones sujetas a reclamo.</li>
                  <li style="padding-left: 20px;">Mantener registros estadísticos a nivel nacional de las solicitudes y consultas del público atendidas por la Superintendencia de Bancos.</li>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/6BhiuGAgs7k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <br>
                <br>
                <b>Contenido:</b> <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO2_ACCESO_A_LA_INFORMACION_EL_CREDITO_Y_EL_ENDEUDAMIENTO_RESPONSABLE.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Generalidades</span></a>
                <br>
                <br>
                <!--<div class="servicio" style="padding: 0.2%;">
                  <div style="text-align: center;">
                    <span style="font-size: 2px;"></span>
                  </div>
                </div>
                <br>-->
                <span class="text-content">Conoce más sobre educación financiera:
                <br>
                <div class="descargas">
                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo1">
                    <span class="">Módulo 1: Seguridad Social</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO1_SEGURIDAD_SOCIAL_PRODUCTOS_Y_SERVICIOS_FINANCIEROS.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>
                    
                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo2">
                    <span class=""> Módulo 2: Acceso a la información</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO2_ACCESO_A_LA_INFORMACION_EL_CREDITO_Y_EL_ENDEUDAMIENTO_RESPONSABLE.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>
                    
                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo3">
                    <span class=""> Módulo 3: Presupuesto familiar</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO1_SEGURIDAD_SOCIAL_PRODUCTOS_Y_SERVICIOS_FINANCIEROS.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo4">
                    <span class="">Módulo 4: Cesantía, Jubilación, Ahorro e inversión</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO4_CESANTIA_JUBILACION_AHORRO_E_INVERSION.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                    <div style="height: 3px;"></div>&nbsp;&nbsp;
                    <a href="<?= URL::base() ?>/educacionfinanciera/modulo5">
                    <span class=""> Módulo 5: Seguridad financiera</span></a>
                    <a target="_blank" href="<?= URL::base() ?>/pdf/MODULO5_SEGURIDAD_FINANCIERA.pdf">
                    <img style="width: 36px;float: right; margin-top: -4px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" ></a>
                    <br><div style="border-bottom: 1px solid #ccc;height: 8px;"></div>

                </div>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>