<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'ASOPREOL | EDUCACIÓN FINANCIERA';
use yii\web\View;
use app\models\Slider;
use app\models\Popup;
$script=<<< JS
var global_brand='';
$(document).ready(function() {
	
});
JS;
$aux='';
$this->registerJs($script,View::POS_END);
?>
<?php
@$info=$_GET['info'];
if (@$info==1){ $infocontent='Gracias por registrarte en el sistema. Se ha enviado un correo electrónico para que puedas acceder a nuestros servicios. '; }
if (@$info==2){ $infocontent='El usuario ingresado ya se encuentra registrado en el sistema.'; }
if (@$info==3){ $infocontent='La contraseña se ha actualizado con éxito. Ahora puede iniciar sesión y acceder a nuestros servicios.'; }
if (@$info==4){ $infocontent='El token generado es inválido. En caso de tener inconvenientes contáctese con nosotros.'; }
?>


 
        <!-- -->


<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>
 
<section class="container-fluid">
    <div class="educacionfinanciera">
        <div class="column1" style="text-align: center;">
            <img style="width: 60%" src="<?= URL::base() ?>/images/site/educacionfinancieraint.fw.png" >
        </div>
        <div class="column2">
            <div style=""><span class="titulo-plan">PLAN</span></div>
            <div style="font-family: 'Arial';text-align: justify; font-size: 12px; color: #595959;">
                <br>
                <span class="titulo-ef">EDUCACIÓN FINANCIERA</span>
                <br>
                <span class="line-center-blue">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <br><br>
                <div style=""><span class="titulo-plan">INTRODUCCIÓN</span></div>
                <span class="text-content">Bienvenidos al programa de educación financiera de “ASOPREOL”, un fondo para brindar un servicio de calidad y con profesionalismo a los partícipes. Este programa brinda la oportunidad de aprender habilidades sobre el manejo de recursos económicos.
                <br><br>
                <div style=""><span class="titulo-plan">OBJETIVO</span></div>
                <span class="text-content">Fortalecer la comprensión de nuestros partícipes sobre los beneficios y servicios financieros que ofrece el mercado, para la toma de decisiones que le permitan mejorar su calidad de vida.
                <br><br>
                <div style=""><span class="titulo-plan">OBJETIVOS ESPECÍFICOS</span></div>
                  <li style="padding-left: 20px;">Transmitir la importancia de la educación financiera y la impresión que causa en la vida futura de la población.</li>
                  <li style="padding-left: 20px;">Brindar asistencia para mejorar la calidad de vida de los partícipes y familia.</li>
                  <li style="padding-left: 20px;">Concientizar el riesgo, derechos y obligaciones, comprendidos en los productos y servicios financieros presentados.</li>
                  <li style="padding-left: 20px;">Incentivar al cambio de óptica sobre el manejo de finanzas en cada uno de sus hogares y obtener mejores resultados a mediano y largo plazo.</li>
                  <li style="padding-left: 20px;">Proporcionar información al público, partícipes de ASOPREOL - FCPCC, respecto a la toma de decisiones para fortalecer la cultura del ahorro en el partícipe para asegurar su futuro y el de su familia.</li>
                <br>
                <!--<div class="servicio" style="padding: 0.2%;">
                  <div style="text-align: center;">
                    <span style="font-size: 2px;"></span>
                  </div>
                </div>
                <br>-->
                <span class="text-content">Conoce más:
                <br>
                <div class="descargas">
                    <a target="_blank" href="<?= URL::base() ?>/pdf/PRESENTACIONEDUCACIONFINANCIERAASOPREOL.pdf"><img style="width: 36px;" src="<?= URL::base() ?>/images/site/pdf_icon.svg" >
                    <span class=""> Plan de Educación Financiera</span></a>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid">
    <div class="servicio" style="padding: 0.5%;">
        <div style="text-align: center;">
            <!--<span style="font-size: 20px;">CESANTÍA</span>
            <div>
                <span class="line-center" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </div>-->
        </div>
    </div>
</section>

<style type="text/css">
.text-content table
{
        width: 100%;
}
.text-content table thead
{
    padding: 2px;
}
.text-content table thead tr th
{
    padding: 2%;
    color: white;
    background-color: #1A185C;
    text-align: center;
    font-size: 14px;
    font-family: 'federo';
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.text-content table tbody tr td
{
    padding: 2%;
    color: black;
    text-align: center;
    font-size: 13px;
    border: 1px solid #1A185C;
    vertical-align: middle;
}
.column1
{
  vertical-align: middle;
  width: 40% !important;
}
.column2
{
  vertical-align: middle !important;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 12% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 26%; /* Could be more or less, depending on screen size */
}

/* The Close Button */
.close {
    color: white;
    float: right;
    font-size: 11px;
    font-weight: bold;
    background: black;
    padding-left: 5px;
    padding-right: 5px;
    opacity: 1;
    height: 15px;
    margin-left: 5px;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>
<?php 

if (@$info || @$popup)
{
	?>
	<script>
		var modal = document.getElementById('myModal');
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

	    var datos= false;
		modal.style.display = "block";

		span.onclick = function() {
	    modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
	</script>
	<?php
}

?>