<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\LoginForm;
use app\models\ContactForm;



class EducacionfinancieraController extends Controller

{
    /**
     * @inheritdoc
     */
    public function behaviors()

    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'logout' => ['post'],
                ],
            ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function actions()

    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
         return $this->render('index');
    }

    public function actionModulo1()
    {
         return $this->render('modulo1');
    }
    public function actionModulo2()
    {
         return $this->render('modulo2');
    }
    public function actionModulo3()
    {
         return $this->render('modulo3');
    }
    public function actionModulo4()
    {
         return $this->render('modulo4');
    }
    public function actionModulo5()
    {
         return $this->render('modulo5');
    }
    public function actionGeneralidades()
    {
         return $this->render('generalidades');
    }
}

